package reqres_user_requester;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import org.json.JSONException;
import org.json.JSONObject;

public class App {

  /**
   * Prints user's full name if this user exists, "User not found!" otherwise.
   * User id must be provided as first command line argument.
   */
  public static void main(String[] args) throws IOException {

    int userId = Integer.parseInt(args[0]);

    JSONObject userInfo = requestUserInfo(userId);
    if (userInfo == null) {
      System.out.println("User not found!");
      return;
    }

    System.out.println(getFullName(userInfo));
  }

  /**
   * Makes an HTTP request to reqres.in and returns user info in JSON format.
   * If response status equals to 404, returns null.
   */
  private static JSONObject requestUserInfo(int userId) throws IOException {
    URL url = new URL("https://reqres.in/api/users/" + String.valueOf(userId));
    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
    connection.setRequestMethod("GET");

    if (connection.getResponseCode() == HttpURLConnection.HTTP_NOT_FOUND) {
      return null;
    }

    BufferedReader in = new BufferedReader(new InputStreamReader(
        connection.getInputStream()));
    String inputLine;
    StringBuffer content = new StringBuffer();

    while ((inputLine = in.readLine()) != null) {
      content.append(inputLine);
    }

    in.close();
    return new JSONObject(content.toString()).getJSONObject("data");
  }

  /**
   * Returns user's full name parsed from its info.
   */
  private static String getFullName(JSONObject userInfo) {
    return userInfo.getString("first_name") + " " + userInfo.getString("last_name");
  }
}