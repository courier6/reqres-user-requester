This app prints full name of selected user from https://reqres.in (try `curl -s "https://reqres.in/api/users/1"`)

To compile, please type
```
mvn package
```

To run app, please type
```
java -jar target/reqres-user-requester-1.0.0.jar <user-id>
```
(`<user-id>` must be integer)